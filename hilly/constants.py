"""

Hilly Defaults

"""
from typing import Final

SRS: Final[str] = "EPSG:6933"
METERS_TO_MILES: Final[float] = 0.0006213712
