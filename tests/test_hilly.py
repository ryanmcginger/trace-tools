"""Test Hilly"""
import os

import pandas as pd
import pytest
from shapely.geometry import box

import hilly

ski_geo_clip = box(-105.885067, 39.682973, -105.879013, 39.686317)
ski_time_clip = (pd.Timestamp("2023-03-12 09:16:48.374000-0600"), pd.Timestamp("2023-03-12 09:30:13.366000-0600"))


def test_io(request):
    ski_fn = os.path.join(request.fspath.dirname, "data", "skiing.gpx")

    df = hilly.io.load_trace(ski_fn)
    df_geo_clip = hilly.io.load_trace(ski_fn, geo_clip=ski_geo_clip)
    df_time_clip = hilly.io.load_trace(ski_fn, time_clip=ski_time_clip)

    assert df.shape[0] > 0
    assert df_geo_clip.shape[0] > 0 and df_geo_clip.shape[0] < df.shape[0]
    assert df_time_clip.shape[0] > 0 and df_time_clip.shape[0] < df.shape[0]


def test_ski(request):
    ski_fn = os.path.join(request.fspath.dirname, "data", "skiing.gpx")

    df = hilly.io.load_trace(ski_fn)
    df = hilly.ski.apply_filters(df)

    summary = hilly.ski.summary(df)

    assert df.labels.min() < 0 and df.labels.max() > 0
    assert summary["ride_ratio"] > 0


def test_rasters(request):
    ski_fn = os.path.join(request.fspath.dirname, "data", "skiing.gpx")

    df = hilly.io.load_trace(ski_fn)

    arr, bounds = hilly.utils.rasterize(df)

    assert arr.shape == (80, 109)
